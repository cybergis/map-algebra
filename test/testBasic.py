import unittest
import os 
import subprocess
import math
from subprocess import check_output

THRESHOLD = 1
Path = os.environ.get("MAPALG_PATH")
mapAlgPath =  Path+"mapalg"
kernelPath = Path+"mapalg-kernel"
def testRun():
    raster1 = "pit.tif"
    raster2 = "pit.tif"
    testRasterRaster(raster1, raster2)
    testRasterNum(raster1, 100)

def testRasterNum(raster1, num):
    addout = "add_{r1}_{r2}.tif".format(r1=raster1.replace(".tif",''), r2=str(num))
    gAddout = "gdal_" + addout
    addCmd = mapAlgPath+" add {r1} {r2} {out}".format(r1=raster1, r2=str(num), out=addout)
    gAddCmd = "gdal_calc.py -A {r1} --outfile={out} --calc=\"A+{r2}\"".format(r1=raster1, r2=str(num), out=gAddout)

    diffout = "diff_{r1}_{r2}.tif".format(r1=raster1.replace(".tif",''), r2=str(num))
    gDiffout = "gdal_" + diffout
    diffCmd = mapAlgPath+" diff {r1} {r2} {out}".format(r1=raster1, r2=str(num), out=diffout)
    gDiffCmd = "gdal_calc.py -A {r1} --outfile={out} --calc=\"A-{r2}\"".format(r1=raster1, r2=str(num), out=gDiffout)

    multout = "mult_{r1}_{r2}.tif".format(r1=raster1.replace(".tif",''), r2=str(num))
    gMultout = "gdal_" + multout
    multCmd = mapAlgPath+" mult {r1} {r2} {out}".format(r1=raster1, r2=str(num), out=multout)
    gMultCmd = "gdal_calc.py -A {r1} --outfile={out} --calc=\"A*{r2}\"".format(r1=raster1, r2=str(num), out=gMultout)

    divout = "div_{r1}_{r2}.tif".format(r1=raster1.replace(".tif",''), r2=str(num))
    gDivout = "gdal_" + divout
    divCmd = mapAlgPath+" div {r1} {r2} {out}".format(r1=raster1, r2=str(num), out=divout)
    gDivCmd = "gdal_calc.py -A {r1} --outfile={out} --calc=\"A/{r2}\"".format(r1=raster1, r2=str(num), out=gDivout)

    compare(addCmd, gAddCmd, addout, gAddout)
    compare(diffCmd,gDiffCmd, diffout, gDiffout)
    compare(multCmd, gMultCmd, multout, gMultout)
    compare(divCmd, gDivCmd, divout, gDivout)

def testRasterRaster(raster1, raster2):
    addout = "add_{r1}_{r2}.tif".format(r1=raster1.replace(".tif",''), r2=raster2.replace(".tif", ""))
    gAddout = "gdal_" + addout
    addCmd = mapAlgPath+" add {r1} {r2} {out}".format(r1=raster1, r2=raster2, out=addout)
    gAddCmd = "gdal_calc.py -A {r1} -B {r2} --outfile={out} --calc=\"A+B\"".format(r1=raster1, r2=raster2, out=gAddout)

    diffout = "diff_{r1}_{r2}.tif".format(r1=raster1.replace(".tif",''), r2=raster2.replace(".tif", ""))
    gDiffout = "gdal_" + diffout
    diffCmd = mapAlgPath+" diff {r1} {r2} {out}".format(r1=raster1, r2=raster2, out=diffout)
    gDiffCmd = "gdal_calc.py -A {r1} -B {r2} --outfile={out} --calc=\"A-B\"".format(r1=raster1, r2=raster2, out=gDiffout)

    multout = "mult_{r1}_{r2}.tif".format(r1=raster1.replace(".tif",''), r2=raster2.replace(".tif", ""))
    gMultout = "gdal_" + multout
    multCmd = mapAlgPath+" mult {r1} {r2} {out}".format(r1=raster1, r2=raster2, out=multout)
    gMultCmd = "gdal_calc.py -A {r1} -B {r2} --outfile={out} --calc=\"A*B\"".format(r1=raster1, r2=raster2, out=gMultout)

    divout = "div_{r1}_{r2}.tif".format(r1=raster1.replace(".tif",''), r2=raster2.replace(".tif", ""))
    gDivout = "gdal_" + divout
    divCmd = mapAlgPath+" div {r1} {r2} {out}".format(r1=raster1, r2=raster2, out=divout)
    gDivCmd = "gdal_calc.py -A {r1} -B {r2} --outfile={out} --calc=\"A/B\"".format(r1=raster1, r2=raster2, out=gDivout)

    compare(addCmd, gAddCmd, addout, gAddout)
    compare(diffCmd,gDiffCmd, diffout, gDiffout)
    compare(multCmd, gMultCmd, multout, gMultout)
    compare(divCmd, gDivCmd, divout, gDivout)

def compare(cmd, gcmd, out, gout):
    print "TEST: " + cmd
    p1 = subprocess.check_output(cmd, shell=True)
    p2 = subprocess.check_output(gcmd, shell=True)
    if diff(out, gout):
        print "TEST: FAILED\n"
    else :
        print "TEST: PASS\n"

def diff(raster1, raster2):
    outfile = "diffResult.tif"
    cmd = "gdal_calc.py -A {r1} -B {r2} --outfile={output} --calc=\"A+1-B\"".format(
        r1 = raster1, r2 = raster2, output=outfile)
    p1 = subprocess.check_call(cmd, shell=True)
    return not withinLimit(outfile)

def withinLimit(filename):
    cmd = "gdalinfo -mm {infile} | grep Computed".format(infile=filename)
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
    out, err = p.communicate()
    minMax = []
    for i in range(len(out)):
        if ord(out[i]) in range(ord('0'), ord('9')+1) or out[i] == '-':
            minMax = out[i:].replace('\n', '').split(',')
            break
    absMax = max(abs(float(minMax[1])),  abs(float(minMax[0])))   
    return abs(absMax - 1) < THRESHOLD 

if __name__ == "__main__":
    testRun()

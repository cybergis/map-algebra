# Map Algebra

Map Algebra is a general-purpose raster manipulation utility combining
the flexibility of GDAL with parallel performance of MPI. Map Algebra
currently provides basic arithematic operations but future versions will
also provide higher level operations.

The use of MPI allows both increased performance and the ability to scale
beyond the memory size available on a single host. Parallel I/O is not
supported so the root processor performs all I/O operations. Future
versions will improve attempt to improve this situation.

## Authors: 
    Yan Y. Liu <yanliu@illinois.edu>
    Garrett Nickel <gmnicke2@illinois.edu>
    Yiming Huang <yhuan125@illinois.edu>
    Mingze Gao <mgao16@illinois.edu>
    Sunwoo Kim <kim392@illinois.edu>
    Jeff Terstriep <jefft@illinois.edu>
	
## Requirements
    gdal >= 1.11.2 (earlier versions not tested but should work)
    MPI >= mpich 3.1.4 (other implementations of MPI should work)
    CMake >= 2.8

## Installation
See the INSTALL file for details but following should work in post cases.
    mkdir build
    cd build
    cmake ..
    make
    make install
    


## Content
- src/
	(parallel map algebra using MPI)
	CMakeList.txt: script to build Makefile using cmake command
	mapalg.cc: main program
	mapalg-kconvolve.cc: map algebra - kernel convolution
	util.{h,cc}: utility functions
	data.{h,cc}: GDAL-based raster I/O functions with data 
		decomposition strategy (block-wise)

	jenkins_build.sh: Provided example Jenkins build script to SSH and use

- test/
	decomp.cc: illustration of how to do block-wise data decomposition 
	rastercp.cc: illustration of using GDAL raster I/O for raster copy

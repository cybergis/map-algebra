/** mapalg.cc: map algebra
 * Author: Mingze Gao <mgao16@illinois.edu>
 * Date: 09/09/2015
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#include <math.h>
#include <errno.h>
#include <unistd.h>
#include <vector>

#include <mpi.h>
#include <gdal.h>
#include <cpl_conv.h>
#include <cpl_string.h>

#include "mapalg.h"
#include "raster.h"
#include "util.h"
#include "data.h"
#include "con.h"
#include "setnull.h"
#include "setnull2.h"
#include "distance.h"
#include "raster_raster.h"
#include "raster_number.h"

int rank, np;
// options
bool perf=false, csv=false;


void MAexit(int retcode) {
    MPI_Finalize();
    exit(retcode);
}


// check whether a file can be accessed by user
bool is_file(string file_name) {
    return (access(file_name.c_str(), F_OK) != -1);
}


/****************************************************************
 * 
 * check if argument can be convert to float number
 * if the argument is a invalid float number return NAN
 * if the converted valid float number is out of range also return NAN
 * otherwise return the converted float number
 */

float fstof(string argument) {
    char * arg = (char*)argument.c_str();
    char *err;
    float d = strtof(arg, &err);  // try to convert str to float
    if (d == (0.0F) && (strlen(err) != 0)) {
        return NAN;  // failed in conversion
    } 
    else {
        if (errno == ERANGE) { // converted value is out of range
	    return NAN;
        }
    }
    return d;
}


inline float plus(float x, float y) {
    return  x+y;
}


inline float minus(float x, float y) {
    return x-y;
}


inline float multiplies(float x, float y) {
    return x*y;
}


inline float divides(float x, float y) {
    return x/y;
}


inline float power(float base, float exponent) {
    return pow(base, exponent);
}


inline float rms_func(float x, float y) {
    return pow(0.5 * (x * x + y * y), 0.5);
}


int distribute_function_call(int argc, std::vector<string> argv, 
			     float (*f)(float, float)) {

    float argv2f = fstof(argv[2]);
    float argv3f = fstof(argv[3]);

    // operation with two number
    if ((!isnan(argv2f)) && (!isnan(argv3f))) {
        if (rank == 0) {
            fprintf(stdout, "%f\n", 
                    (*f)(strtof(argv[2].c_str(),NULL),strtof(argv[3].c_str(), NULL)));
        }
    }

    // operation of two raster
    else if (is_file(argv[2]) && is_file(argv[3])) {
	raster_raster((char*)argv[2].c_str(), (char*)argv[3].c_str(), (char*)argv[4].c_str(), 
		      f);
    }

    // operation of number with raster
    else if (!isnan(argv2f) && is_file(argv[3])) {
	raster_number((char*)argv[3].c_str(), argv2f, (char*)argv[4].c_str(), f, false);
    }
	
    // operation of raster with number
    else if (!isnan(argv3f) && is_file(argv[2])) {
	raster_number((char*)argv[2].c_str(), argv3f, (char*)argv[4].c_str(), f, true);
    }
    else {
	return -1;
    }
}


void sum(int argc, std::vector<string> argv) {
    char usage[] = "add <raster|number> "
	"<raster|number> <out>";
    if(argc != 5) {
	char error[] = "Invalid number of argument";
	MAerror("%s", error);
	MAusage("%s",usage);
	MAexit(-1);
    }
    int err = distribute_function_call(argc, argv, plus);
    if(err == -1) {
	char error[] = "please check your arguments";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);
    }
}


void diff(int argc, std::vector<string> argv) {
    char usage[] = "Usage: diff <raster|number> "
	"<raster|number> <out>\n";
    if(argc != 5) {
	char error[] = "Invalid number of argument\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
    int err = distribute_function_call(argc, argv, minus);
    if(err == -1) {
	char error[] = "please check your arguments\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
}


void mult(int argc, std::vector<string> argv) {
    char usage[] = "Usage: mult <raster|number> "
	"<raster|number> <out>\n";
    if(argc != 5) {
	char error[] = "Invalid number of argument\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
    int err = distribute_function_call(argc, argv, multiplies);
    if(err == -1) {
	char error[] = "please check your arguments\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);
    }
}


void div(int argc, std::vector<string> argv) {
    char usage[] = "Usage: div <raster|number> "
	"<raster|number> <out>\n";
    if(argc != 5) {
	char error[] = "Invalid number of argument\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
    int err = distribute_function_call(argc, argv, divides);
    if(err == -1) {
	char error[] = "please check your arguments\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);


    }
}


void exp(int argc, std::vector<string> argv) {
    char usage[] = "Usage: exp <raster|number> "
	"<raster|number> <out>\n";
    if(argc != 5) {
	char error[] = "Invalid number of argument\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
    int err = distribute_function_call(argc, argv, power);
    if(err == -1) {
	char error[] = "please check your arguments\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
}


void sqrt(int argc, std::vector<string> argv) {
    char usage[] =  "Usage: [options] sqrt <raster|number> <out>\n";

    if(argc != 4) {
	char error[] = "Invalid number of argument\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
    string point5 = "0.5";
    std::vector<string> updated_argv = argv;
    updated_argv[4] = argv[3];
    updated_argv[3] = point5;
    int err = distribute_function_call(5, updated_argv, power);
    if(err == -1) {
	char error[] = "please check your arguments\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
}



void rms(int argc, std::vector<string> argv) {
    char usage[] =  "Usage: [options] rms <raster1> "
	"<raster2> <out>\n";
    if(argc != 5) {
	char error[] = "Invalid number of argument\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
    int err = distribute_function_call(argc, argv, rms_func);
    if(err == -1) {
	char error[] = "please check your arguments\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
}

void dist(int argc, std::vector<string> argv) {
    char usage[] = "Usage: dist <raster> <x> <y> <z> <out>\n";

    float argv3f = fstof(argv[3]);
    float argv4f = fstof(argv[4]);
    float argv5f = fstof(argv[5]);

    if (argc != 7) {
	char error[] =  "Invalid number of argument\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    }
    else if (isnan(argv3f) || isnan(argv4f) || isnan(argv5f)) {
	char error[] = "Please check your arguments\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);
    } 
    else {
	distance((char*)argv[2].c_str(), argv3f, argv4f,
		 argv5f, (char*)argv[6].c_str());
    }
}

void setnull(int argc, std::vector<string> argv) {
    char usage [] = "Usage: setnull <raster> <false val> "
	"<condition operator> <condition val> <out>\n";
    float argv3f = fstof(argv[3]);
    float argv5f = fstof(argv[5]);

    if (argc != 7) {
	char error[] = "Invalid number of argument\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);
    }

    else if (isnan(argv3f) || isnan(argv5f)) {
       
	char error[] =  "Please check your arguments\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);


    } else if (argv[4] != "gt" && argv[4] != "ge"
	       && argv[4] != "lt" && argv[4] != "le") {
	char error[] = "The condition operator is not valid!\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    } else {
	float con_val = argv5f;
	char *con_op = (char*)argv[4].c_str();
	char * infn = (char*)argv[2].c_str();
	float value = argv3f;
	char * outfn = (char*)argv[6].c_str();
	setnull_proc(infn, value, con_op, con_val, outfn);
    }
}


void setnull2(int argc, std::vector<string> argv) {
    char  usage [] = "Usage: setnull2 <raster1> <raster2> "
	"<condition operator> <condition val> <out>\n";

    if (argc != 7) {
	char error[] = "Invalid number of argument\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    } 

    else if (isnan(fstof(argv[5]))) {
	MAerror("Invalid argument %s, must be a float number", argv[5].c_str());
	MAusage("%s", usage);
	MAexit(-1);
    } 

    else if (argv[4] != "gt" && argv[4] != "ge"
	     && argv[4] != "lt" && argv[4] != "le") {
	MAerror("Conditon operator is not valid %s", argv[4].c_str());
	MAusage("%s", usage);
	MAexit(-1);
	
    } 
    
    else {
	float con_val = fstof(argv[5]);
	char *con_op = (char*)argv[4].c_str();
	char * infn = (char*)argv[2].c_str();
	char * infn2 = (char*)argv[3].c_str();
	char * outfn = (char*)argv[6].c_str();
	setnull2_proc(infn, infn2, con_op, con_val, outfn);
    }
}

void con(int argc, std::vector<string> argv) {
    char  usage [] = "Usage: con <raster> <condition operator> "
	"<condition val> <value> <out>\n";
    float argv4f = fstof(argv[4]);
    float argv5f = fstof(argv[5]);
    
    if (argc != 7) {
	char error[] = "Invalid number of argument\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);

    } 
    else if (isnan(argv4f) || isnan(argv5f)) {
	char error[] = "please check your arguments\n";
	MAerror("%s", error);
	MAusage("%s", usage);
	MAexit(-1);


    } else if (argv[3] != "gt" && argv[3] != "ge"
	       && argv[3] != "lt" && argv[3] != "le") {
	MAerror("Conditon operator is not valid %s", argv[3].c_str());
	MAusage("%s", usage);
	MAexit(-1);

    } else {
	float con_val = argv4f;
	char *con_op = (char*)argv[3].c_str();
	char * infn = (char*)argv[2].c_str();
	float value = argv5f;
	char * outfn = (char*)argv[6].c_str();
	con_proc(infn, con_op, con_val, value, outfn);
    }
}


/****************************************************************
 *
 * Gather all options to  a string vector 
 */
std::vector < std::string > constructOptions(int * argc, char ** &argv) {
    std::vector < std::string > options;
    char ** updatedArgv;
    for (int i = 0; i < *argc; i++) {
        if(*argv[i] == '-') {
            options.push_back(argv[i]);
        }
    }
    return options;
}


/****************************************************************
 *
 * Gather all arguments to a string vector and update argc and argv
 */
std::vector <std::string> constructArgs(int * argc, char ** &argv) {
    std::vector<std::string> arguments;
    char **updatedArgv;
    for(int i = 0; i < * argc; i++) {
	if(*argv[i] != '-') {
	    arguments.push_back(argv[i]);
	}
    }
    updatedArgv = (char**)malloc(sizeof(updatedArgv)* (*argc));
    for(int i = 0; i < arguments.size(); i++) {
        updatedArgv[i] = (char*)malloc(arguments[i].length());
        strcpy(updatedArgv[i], arguments[i].c_str());
    }
    argv = updatedArgv;
    *argc = arguments.size();
    return arguments;
}

/****************************************************************
 *
 * Parse the command line options specific to Map Algebra
 *
 * parse_options should be called after MPI_Init which will
 * MPI options after processing. Similarly parse_options should
 * remove any options found and resulting argv should have
 * the method as argv[1].
 *
 * Note: modifying the argv is a questionable practice, but MPI_Init
 * does it so I guess we can too.
 */
std::vector<std::string> parse_options(int *argc, char** &argv) {
    std::vector<std::string> options = constructOptions(argc, argv);
    for(int i = 0; i< options.size(); i++) {
	if (!strcmp(options[i].c_str(), "-h") || !strcmp(options[i].c_str(), "--help")) {
	    if (rank == 0)
		MAhelp();
	    MAexit(0);
	}

	else if (!strcmp(options[i].c_str(), "--perf")) {
	    perf = true;
	}

	else if (!strcmp(options[i].c_str(), "--csv")) {
	    csv = true;
	}
	
	else  {
	    MAerror("unknown option %s", options[i].c_str());
	    MAhelp();
	    MAexit(-1);
	}
    }
    return constructArgs(argc, argv);
}


/****************************************************************
 *
 * Method Switch checks the remaining command line argumment and
 * passes control to appropriate method to handle the reading of
 * raster files, specific processing, and writing of results.
 */
void method_switch(std::vector<std::string> argv) {
    string method;
    int argc = argv.size();
    if(argc < 2) {
	MAhelp();
	MAexit(1);
    }
    method = argv[1];

    if (method == "add") {
	sum(argc, argv);
    }

    else if (method == "diff") {
	diff(argc, argv);
    }

    else if (method == "mult") {
	mult(argc, argv);
    }

    else if (method == "div") {
	div(argc, argv);
    }

    else if (method == "exp") {
	exp(argc, argv);
    } else if (method == "rms") {
	rms(argc, argv);
    }

    else if (method == "dist") {
	dist(argc, argv);
    } else if (method == "sqrt") {
	sqrt(argc, argv);
    }

    else if (method == "setnull") {
	setnull(argc, argv);
    }

    else if (method == "setnull2") {
	setnull2(argc, argv);
    }

    else if (method == "con") {
	con(argc, argv);
    }

    else {
	if (rank == 0) {
	    fprintf(stderr, "Unknown method %s\n", argv[1].c_str());
	}
	MPI_Finalize();
	exit(1);
    }
}

// print help information
void MAhelp() {
    const char *help_info =
	"Usage: mapalg [options] <method> <args> [<args>...] <output>\n"
	"\nOptions:\n"
	"  --help, -h   : this help section\n"
	"  --perf       : performance information written to stdout\n"
	"  --csv        : performance written to stdout in CSV format\n"
	"\nMethods:\n"
	"  add | diff | mult | div | exp "
	"<raster|number> <raster|number> <output>\n"
	"               : outputs the basic cell-wise algebraic operations\n"
	"                 between input1 and input2 where inputs may be \n"
	"                 either a raster file or a number\n\n"
	"  sqrt <raster>\n"
	"               : outputs the square root of input raster\n\n"
	"  rms <raster1> <raster2>\n"
	"               : outputs the root mean square of input rasters\n\n"
	"  dist <raster> <x> <y> <z>\n"
	"               : outputs the distance between each cell and point\n"
	"                 where x, y, and z are given in the units of the\n"
	"                 input raster\n\n"
	"  setnull <raster> <false val> <con op> <con val>\n"
	"               : output valid cell values to null and the false\n"
	"                 value is from a constant\n\n"
	"  setnull2 <raster> <raster> <con op> <con val>\n"
	"               : set valid cell values to null and the false value\n "
	"                 is from a raster\n\n"
	"  con <raster> <con op> <con val> <value>\n"
	"               : set valid cell values to a constant\n\n";
    
    if(rank == 0) {
	fprintf(stdout, help_info);
    }
}


// assumption: float value; same dimensions
int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &np);
    std::vector<string> args = parse_options(&argc, argv);
    method_switch(args);

    MPI_Finalize();
}

#include "raster_raster.h"

// raster raster operations
void raster_raster(char* raster1, char* raster2, char* output_file, 
		   float (*f)(float, float)) {
    Raster r1, r2;
    Block b1, b2;
    double t0, t1, t2, t3, t4, t5;
    t0 = get_timemark();
    // step 1 : read input raster
    populate_raster(r1, raster1);
    populate_raster(r2, raster2);
    t1 = get_timemark();
    // step 2 : transfer data blocks to procs
    raster_to_block(b1, r1);
    raster_to_block(b2, r2);
    MAdebugAll("rank %d: max[x,y]=%d,%d nodata=%.5lf size=%d,%d\n", rank,
	       b1.maxx, b1.maxy, b1.nodata, b1.bsizex, b1.bsizey);

    t2 = get_timemark();
    // map algebra operation
    int index;
    for(int i=0; i<b1.bsizey; i++) {
        for(int j=0; j<b1.bsizex; j++) {
            index = i * b1.bsizex + j;
            if(fabs((double)(b1.data[index]) - b1.nodata) < 0.001)
                b1.data[index] = b1.nodata;
            else
                b1.data[index] = (*f)(b1.data[index], b2.data[index]);
        }
    }
    t3 = get_timemark();

    // step4 : transfer results for writing
    block_to_raster(b1, r1);
    t4 = get_timemark();

    // step5 : write output
    if (rank == 0)
        write_raster(r1, output_file);
    t5 = get_timemark();

    if (perf && rank == 0)
        show_perf(t0, t1, t2, t3, t4, t5, csv);

    // step6 : clean up
    clean_raster(r1);
    clean_raster(r2);
    clean_block(b1);
    clean_block(b2);
}


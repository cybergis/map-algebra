#ifndef SETNULL_H
#define SETNULL_H

#include "function_util.h"

void setnull_proc(char* raster1, float value, char *con_op, float con_val,
                  char* output_file);

#endif 

#include "distance.h"

void distance(char* raster1, float xp, float yp, float zp,  char* output_file) {
    Raster r1;
    Block b1;
    double t0, t1, t2, t3, t4, t5;
    t0 = get_timemark();
    // step1 : read input raster
    if (rank == 0) {
        populate_raster(r1, raster1);
    }
    t1 = get_timemark();
    // step2 : transfer data blocks to procs
    raster_to_block(b1, r1);
    MAdebugAll("rank %d: max[x,y]=%d,%d nodata=%.5lf size=%d,%d\n", rank,
	       b1.maxx, b1.maxy, b1.nodata, b1.bsizex, b1.bsizey);

    t2 = get_timemark();
    // step3 : map algebra operation
    int index;
    float xc, yc, zc;
    for(int i=0; i<b1.bsizey; i++) {
        for(int j=0; j<b1.bsizex; j++) {
            index = i * b1.bsizex + j;
            if(fabs((double)(b1.data[index]) - b1.nodata) < 0.001) continue;
            xc = r1.georef[0] + (b1.boffsetx + j) * r1.georef[1];
            yc = r1.georef[3] + (b1.boffsety + i) * r1.georef[5];
            zc = b1.data[index];
            b1.data[index] = get_dist(xc, yc, zc, xp, yp, zp);
        }
    }
    t3 = get_timemark();
    // step4 : transfer results for writing
    block_to_raster(b1, r1);
    t4 = get_timemark();
    // step5 : write output
    if (rank == 0) {
        write_raster(r1, output_file);
    }
    t5 = get_timemark();
    if (perf && rank==0)
        show_perf(t0, t1, t2, t3, t4, t5, csv);
    clean_raster(r1);
    clean_block(b1);
}

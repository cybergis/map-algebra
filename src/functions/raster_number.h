#ifndef RASTER_NUMBER_H
#define RASTER_NUMBER_H

#include "function_util.h"

void raster_number(char* raster1, float num, char* output_file, float (*f)(float, float),
                   bool regular_order);
#endif

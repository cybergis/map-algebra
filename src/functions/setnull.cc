#include "setnull.h"

void setnull_proc(char* raster1, float value, char *con_op, float con_val,
                  char* output_file) {
    Raster r1;
    Block b1;
    double t0, t1, t2, t3, t4, t5;
    t0 = get_timemark();

    // step1 : read input raster
    if (rank == 0) {
        populate_raster(r1, raster1);
    }
    t1 = get_timemark();

    // step2 : transfer data blocks to procs
    raster_to_block(b1, r1);
    MAdebugAll("rank %d: max[x,y]=%d,%d nodata=%.5lf size=%d,%d\n", rank,
	       b1.maxx, b1.maxy, b1.nodata, b1.bsizex, b1.bsizey);
    t2 = get_timemark();

    // step3 : map algebra operation
    int index;
    for (int i=0; i<b1.bsizey; i++) {
        for (int j=0; j<b1.bsizex; j++) {
            index = i * b1.bsizex + j;
            if (fabs((float)(b1.data[index]) - b1.nodata) < 0.001)
                continue;
            else {
                if (strcmp(con_op,"gt")==0) {
                    if (b1.data[index] > con_val)
                        b1.data[index] = b1.nodata;
                    else
                        b1.data[index] = value;
                } else if (strcmp(con_op,"ge")==0) {
                    if (b1.data[index] >= con_val)
                        b1.data[index] = b1.nodata;
                    else
                        b1.data[index] = value;
                } else if (strcmp(con_op,"lt")==0) {
                    if (b1.data[index] < con_val)
                        b1.data[index] = b1.nodata;
                    else
                        b1.data[index] = value;
                } else if (strcmp(con_op,"le")==0) {
                    if (b1.data[index] <= con_val)
                        b1.data[index] = b1.nodata;
                    else
                        b1.data[index] = value;
                }
            }
        }
    }

    t3 = get_timemark();
    // step4 : transfer results for writing
    block_to_raster(b1, r1);
    t4 = get_timemark();
    // step5 : write output
    if (rank == 0) {
        write_raster(r1, output_file);
    }
    t5 = get_timemark();
    if (perf && rank==0)
        show_perf(t0, t1, t2, t3, t4, t5, csv);
    clean_raster(r1);
    clean_block(b1);
}


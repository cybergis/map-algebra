#ifndef RASTER_H
#define RASTER_H

#include <gdal.h>
#include <cpl_conv.h>
#include <cpl_string.h>
#include <mpi.h>

#include "data.h"
#include "util.h"

#ifdef DEBUG
#  define DBGMSG 1
#else
#  define DBGMSG 0
#endif


extern int rank, np;
extern bool perf, csv;

#define MAdebug(fmt, ...)						\
    do { if (DBGMSG && !rank) fprintf(stderr, "%s:%d:%s(): " fmt,	\
				      __FILE__, __LINE__, __func__, ##__VA_ARGS__); } while(0)


#define MAdebugAll(fmt, ...)						\
    do { if (DBGMSG) fprintf(stderr, "%s:%d:%s(): " fmt,		\
			     __FILE__, __LINE__, __func__, ##__VA_ARGS__); } while(0)


// each process is assigned with one block
typedef struct {
    int maxx, maxy;
    int bsizex, bsizey;
    int boffsetx, boffsety;
    double nodata;
    float* data;
} Block;

// block topo information
typedef struct {
    int *offsetx, *offsety;
    int *sizex, *sizey;
    int maxx, maxy;
} Position;

// only root process handle the Raster
typedef struct {
    double georef[6]; // georef data struct for a raster
    char prj[2048];   // store projection wkt
    double nodata;
    int x, y; // size of raster on x and y dim
    float *data;
    Position p;
} Raster;

void clean_raster(Raster &raster);
void clean_block(Block &b);
void write_raster(Raster &raster, char* output_file_name);
void populate_raster(Raster &raster, char* raster_file);
void position(Position &p, int x, int y);
void raster_to_block(Block &block,Raster &raster);
void block_to_raster(Block &block, Raster &raster);

#endif


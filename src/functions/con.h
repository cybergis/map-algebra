#ifndef CON_H
#define CON_H

#include "function_util.h"

void con_proc(char* raster1, char *con_op, float con_val, float value,
              char* output_file);
#endif 

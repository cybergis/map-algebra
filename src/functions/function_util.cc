#include "function_util.h"

// write performation information to stdou
void show_perf(double t0, double t1, double t2,double t3, double t4, double t5, float csv) {
    jobstat.Tread = t1 - t0;
    jobstat.Tcommdata = t2 - t1;
    jobstat.Tcompute = t3 - t2;
    jobstat.Tcommresult = t4 - t3;
    jobstat.Twrite = t5 - t4;
    jobstat.Ttotal = t5 - t0;
    if(csv)
        print_jobstat_csv();
    else
        print_jobstat();
}

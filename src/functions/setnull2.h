#ifndef SETNULL2_H
#define SETNULL2_H

#include "function_util.h"

void setnull2_proc(char* raster1, char * raster2, char *con_op, float con_val,
                   char* output_file);

#endif 

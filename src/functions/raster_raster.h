#ifndef RASTER_RASTER_H
#define RASTER_RASTER_H

#include "function_util.h"

void raster_raster(char* raster1, char* raster2, char* output_file, 
		   float (*f)(float, float));

#endif 

#include "raster.h"

// free allocated memory of a Raster
void clean_raster(Raster &raster) {
    // only root raster has real data
    if(rank == 0) {
        free(raster.data);
        free(raster.p.sizex);
        free(raster.p.sizey);
        free(raster.p.offsetx);
        free(raster.p.offsety);
    }
}

void clean_block(Block &b) {
    free(b.data);
}

// create output raster file and write result
void write_raster(Raster &raster, char* output_file_name) {
    GDALDatasetH rout;
    float *block;
    rout = raster_create(output_file_name, raster.x, raster.y,
                         raster.georef, raster.prj, raster.nodata);
    for(int i=0; i<np; i++) {
        block = raster.data + i * (raster.p.maxx *raster.p.maxy);
        raster_write(rout, block, raster.p.offsetx[i], raster.p.offsety[i],
                     raster.p.sizex[i], raster.p.sizey[i]);
    }
    raster_close(rout);
}

// root process reads dataset
void populate_raster(Raster &raster, char* raster_file, bool debug) {
    GDALDatasetH rin;
    float* block;
    rin = raster_open(raster_file, raster.georef, raster.prj,
                      &(raster.nodata), &(raster.x), &(raster.y));
    if(debug && rank == 0) {
        raster_info(rin, raster.georef, raster.prj, raster.nodata,
        raster.x, raster.y);
    }
    if(rank == 0) {
        position(raster.p, raster.x, raster.y);
        raster.data = (float *)malloc(sizeof(float)
                                      * raster.p.maxx * raster.p.maxy * np);
        memset(raster.data, 0, sizeof(float)
                                      * raster.p.maxx * raster.p.maxy * np);
        for(int i=0; i<np; i++) {
            block = raster.data + i * (raster.p.maxx * raster.p.maxy);
            raster_read(rin, block, raster.p.offsetx[i], raster.p.offsety[i],
                        raster.p.sizex[i], raster.p.sizey[i]);
        }
    }
    raster_close(rin);
}

// root process determine block sizes
void position(Position &p, int x, int y, int np) {
    p.maxx=0, p.maxy=0;
    p.offsetx = (int *) malloc(sizeof(int) * np);
    memset(p.offsetx, 0, sizeof(int) * np);
    p.offsety = (int *) malloc(sizeof(int) * np);
    memset(p.offsety, 0, sizeof(int) * np);
    p.sizex = (int *) malloc(sizeof(int) * np);
    memset(p.sizex, 0, sizeof(int) * np);
    p.sizey = (int *) malloc(sizeof(int) * np);
    memset(p.sizey, 0, sizeof(int) * np);
    for (int i=0; i<np; i++) {
        get_block(i, np, x, y, &(p.offsetx[i]), &(p.offsety[i]),
                  &(p.sizex[i]), &(p.sizey[i]));
    }
    // find max sizex and sizey
    for (int i=0; i<np; i++) {
        if (p.maxx < p.sizex[i]) p.maxx = p.sizex[i];
        if (p.maxy < p.sizey[i]) p.maxy = p.sizey[i];
    }
}


// root proc gathers results
void block_to_raster(Block &block, Raster &raster) {
    MPI_Gather(block.data, block.maxx*block.maxy, MPI_FLOAT, raster.data,
               block.maxx*block.maxy, MPI_FLOAT, 0, MPI_COMM_WORLD);
    // MPI_Barrier(MPI_COMM_WORLD);
}

// root proc transfer blocks to other proc
void raster_to_block(Block &block,Raster &raster) {
    block.maxx = raster.p.maxx;
    block.maxy = raster.p.maxy;
    block.nodata = raster.nodata;
    MPI_Bcast(&(block.maxx), 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&(block.maxy), 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&(block.nodata), 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Scatter(raster.p.offsetx, 1, MPI_INT, &(block.boffsetx),
                1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(raster.p.offsety, 1, MPI_INT, &(block.boffsety),
                1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(raster.p.sizex, 1, MPI_INT, &(block.bsizex),
                1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(raster.p.sizey, 1, MPI_INT, &(block.bsizey),
                1, MPI_INT, 0, MPI_COMM_WORLD);
    block.data = (float *) malloc(sizeof(float)*block.maxx*block.maxy);
    MPI_Scatter(raster.data, block.maxx * block.maxy, MPI_FLOAT, block.data,
                block.maxx*block.maxy, MPI_FLOAT, 0, MPI_COMM_WORLD);
}

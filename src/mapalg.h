#ifndef MAPALG_H
#define MAPALG_H

#include <string>

using std::string;

extern int rank, np;
extern bool  perf, csv;
#ifdef DEBUG
#  define DBGMSG 1
#else
#  define DBGMSG 0
#endif

#define MAdebug(fmt, ...)						\
    do { if (DBGMSG && !rank) fprintf(stderr, "%s:%d:%s(): " fmt,	\
				      __FILE__, __LINE__, __func__, ##__VA_ARGS__); } while(0)

#define MAdebugAll(fmt, ...)						\
    do { if (DBGMSG) fprintf(stderr, "%s:%d:%s(): " fmt,		\
			     __FILE__, __LINE__, __func__, ##__VA_ARGS__); } while(0)

#define MAerror(fmt, ...)						\
    do { if (!rank) fprintf(stderr, "Error: " fmt "\n",##__VA_ARGS__); } while(0)

#define MAusage(fmt, ...)						\
    do {if (!rank) fprintf(stderr, "Usage: " fmt "\n",##__VA_ARGS__); } while(0)



void MAhelp();
void MAexit(int retcode);
bool is_file(string file_name);
float fstof(string argument);
int  distribute_function_call(int argc, std::vector<string> argv,
			      float (*f)(float, float));
#endif

